const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const sortJsonArray = require('sort-json-array');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( {extended: false}));

const baseSwapiURL = 'https://swapi.dev/api'

let people = [];
let planets = [];


// loadPeople loads api data into memory for faster retrieval and manipulation
const loadPeople = async () => {
    people = await getAll(baseSwapiURL + '/people/')
    console.log("loaded all people")
}

// loadPlanets loads api data into memory for faster retrieval and manipulation
const loadPlanets = async () => {
    planets = await getAll(baseSwapiURL + '/planets/')
    getResidents()
    console.log("loaded all planets")
}

// getResidents replaces the url for the residents with their names
const getResidents = () => {
    planets.forEach(planet => {
        let residents = []
        planet.residents.forEach(residentURL => {

            axios.get(residentURL).
                then((response) => {
                    residents.push(response.data.name)
                }, (error) => {
                    console.log(error)
                });

        })
        planet.residents = residents
    });
}

// getAll retrieves all data 
const getAll = async (query) => {
    const response = await axios.get(query)
    const data = response.data

    if(data.next == null) {
        return data.results
    }
    else {
        return data.results.concat(await getAll(data.next))
    }
}

const sortArray = (array, sortBy) => {
    console.log("sorting array by: " + sortBy)
    
    // sorting a copy instead of raw data to preserve original ordering
    return sortJsonArray([...array], sortBy); 
}

// Setup: load api data into memory
loadPeople()
loadPlanets()
// END setup


// REST endpoints
app.get('/people/:sortBy?', async (req, res) => {
    res.send(sortArray(people, req.query.sortBy))
})

app.get('/planets', async (req, res) => {
    res.send(planets)
})

// END Endpoints

app.listen(3000, () => console.log('Server listening on port 3000'))
